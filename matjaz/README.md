# Problem reduction technique

With problem reduction technique, the input problem instance is reduced in the number of elements and the number of candidate sets with the aim to create a reduced problem instance that could be solved faster. 

The problem reduction approach aims to divide and conquer the input problems that are too large to be approached with an exact solver. The approach divides the problem into chunks, where each chunk only considers a subset of elements of the input problem. Subsets of elements are selected according the subset size which is an input parameter to solver.

The procedure is as follows. 
1. A seed element is selected; this will be the element from which the reduced problem will _grow_. Current implementation uses the unused element with lowest index as the seed.
2. Seed element is marked as _active_ and then the subproblem is grown from the seed in iterative fashion, until the number of elements included in the subproblem reaches the target number. 
  - The unused elements from the input problem, that are covered by at least one of the subproblem candidate sets are added to the subproblem one by one. Note that in first iteartion, the subproblem holds no candidate sets and tihs step is simply skipped. The order in which the elements are added is determined by a greedy heuristic - those that are covered by the least additional candidate sets are added first. If the target number of elements is reached, this step ends early.
  - All the candidate sets from input problem that cover at least one of active elements are added to the subproblem.
3. The subproblem is solved using the approximate algorithm. Since the subproblem can be orders of magnitude smaller (in the number of candidate sets and elements), even the exact solution becomes feasible.
4. The elements covered by the candidate sets used in the subproblem solution are flagged as used and a new iteration of the soluiton procedure starts while some unused elements remain.

While this procedure greatly reduces the problem complexity, it does it on the account of exactness. While each subproblem may be solved exactly, and the union of subproblem solutions forms a valid full solution, the final solution is not exact. 

Graphical representation of several steps of the algorithm. The number of elements to form a subproblem is set to 50% of all elements in this example.

The whole problem comprises elements (small blue discs) and candidate sets (black circles) that cover several elements each.
![](Illustrations/input_problem.png) 

The subproblem is defined by selecting a subset of elements. The illustration shows the selected elements colored red.
![](Illustrations/subproblem_selected_elements.png) 

The subproblem also contains a subset of candidate sets, which is shown in the following illustration as red. Note that all candidates that cover at least one selected element are selected but not all the covered elements by these candidates are selected.
![](Illustrations/subproblem_fully_defined.png) 

Then the subproblem is solved - a valid cover is found. The illustration shows the found cover colored green. Note that more elements may be covered than were selected for a subproblem, since some of the used candidate sets may cover additional elements, that were not selected for the subproblem.
![](Illustrations/subproblem_solved.png) 

Then the iterative covering approach continues with the second (and final in this example) iteration. All the remaining (unused, that is, not covered green) elements are selected and all the unused candidate sets that cover at least one selected element are selected (colored red).
![](Illustrations/subproblem2_fully_defined.png) 

When the final iteration is solved, the combined solution is a valid solution of the input problem. That is, all the input elements are covered and only a subset of candidate sets might be used to form the cover. In this illustration, all the candidates that form the solution are colored green, and the rest are colored black.
![](Illustrations/problem_solved.png) 

